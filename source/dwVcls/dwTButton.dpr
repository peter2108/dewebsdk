﻿library dwTButton;

uses
    ShareMem,

    //
    dwCtrlBase,

    //
    SynCommons,

    //
    Messages, SysUtils, Variants, Classes, Graphics,
    Controls, Forms, Dialogs, ComCtrls, ExtCtrls,
    StdCtrls, Windows;


//--------------------------------------------------------------------------------------------------

function _GetFont(AFont:TFont):string;
begin
     Result    := 'color:'+dwColor(AFont.color)+';'
               +'font-family:'''+AFont.name+''';'
               +'font-size:'+IntToStr(AFont.size+3)+'px;';

     //粗体
     if fsBold in AFont.Style then begin
          Result    := Result+'font-weight:bold;';
     end else begin
          Result    := Result+'font-weight:normal;';
     end;

     //斜体
     if fsItalic in AFont.Style then begin
          Result    := Result+'font-style:italic;';
     end else begin
          Result    := Result+'font-style:normal;';
     end;

     //下划线
     if fsUnderline in AFont.Style then begin
          Result    := Result+'text-decoration:underline;';
          //删除线
          if fsStrikeout in AFont.Style then begin
               Result    := Result+'text-decoration:line-through;';
          end;
     end else begin
          //删除线
          if fsStrikeout in AFont.Style then begin
               Result    := Result+'text-decoration:line-through;';
          end else begin
               Result    := Result+'text-decoration:none;';
          end;
     end;
end;
function _GetFontWeight(AFont:TFont):String;
begin
     if fsBold in AFont.Style then begin
          Result    := 'bold';
     end else begin
          Result    := 'normal';
     end;

end;
function _GetFontStyle(AFont:TFont):String;
begin
     if fsItalic in AFont.Style then begin
          Result    := 'italic';
     end else begin
          Result    := 'normal';
     end;
end;
function _GetTextDecoration(AFont:TFont):String;
begin
     if fsUnderline in AFont.Style then begin
          Result    :='underline';
          //删除线
          if fsStrikeout in AFont.Style then begin
               Result    := 'line-through';
          end;
     end else begin
          //删除线
          if fsStrikeout in AFont.Style then begin
               Result    := 'line-through';
          end else begin
               Result    := 'none';
          end;
     end;
end;

//--------------------------------------------------------------------------------------------------

//当前控件需要引入的第三方JS/CSS
function dwGetExtra(ACtrl:TComponent):String;stdCall;
begin
     Result    := '[]';
end;

//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):String;StdCall;
var
     joData    : Variant;
begin

     //
     joData    := _Json(AData);

     if joData.e = 'onclick' then begin
          if Assigned(TButton(ACtrl).OnClick) then begin
               TButton(ACtrl).OnClick(TButton(ACtrl));
          end;
     end else if joData.e = 'onenter' then begin
          if Assigned(TButton(ACtrl).OnEnter) then begin
               TButton(ACtrl).OnEnter(TButton(ACtrl));
          end;
     end else if joData.e = 'onexit' then begin
          if Assigned(TButton(ACtrl).OnExit) then begin
               TButton(ACtrl).OnExit(TButton(ACtrl));
          end;
     end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):String;StdCall;
var
    sCode   : String;
    sRIcon  : string;

    //
    joHint  : Variant;
    joRes   : Variant;
    sEnter  : String;
    sExit   : String;
    sClick  : string;
begin

    //生成返回值数组
    joRes    := _Json('[]');
    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    //_DWEVENT = ' @%s="dwevent($event,''%s'',''%s'',''%s'',''%d'')"';
    //参数依次为: JS事件名称, 控件名称,控件值,Delphi事件名称,句柄


    //
    with TButton(ACtrl) do begin

        //进入事件代码----------------------------------------------------------------------------
        sEnter  := '';
        if joHint.Exists('onenter') then begin
            sEnter  := 'dwexecute('''+joHint.onenter+''');';    //取得OnEnter的JS代码
        end;
        if sEnter='' then begin
            if Assigned(OnEnter) then begin
                sEnter    := Format(_DWEVENT,['mouseenter.native',Name,'0','onenter',TForm(Owner).Handle]);
            end else begin
            end;
        end else begin
            if Assigned(OnEnter) then begin
                sEnter    := Format(_DWEVENTPlus,['mouseenter.native',sEnter,Name,'0','onenter',TForm(Owner).Handle])
            end else begin
                sEnter    := ' @mouseenter.native="'+sEnter+'"';
            end;
        end;


        //退出事件代码----------------------------------------------------------------------------
        sExit  := '';
        if joHint.Exists('onexit') then begin
            sExit  := 'dwexecute('''+joHint.onexit+''');';
        end;
        if sExit='' then begin
            if Assigned(OnExit) then begin
                sExit    := Format(_DWEVENT,['mouseleave.native',Name,'0','onexit',TForm(Owner).Handle]);
            end else begin
            end;
        end else begin
            if Assigned(OnExit) then begin
                sExit    := Format(_DWEVENTPlus,['mouseleave.native',sExit,Name,'0','onexit',TForm(Owner).Handle])
            end else begin
                sExit    := ' @mouseleave.native="'+sExit+'"';
            end;
        end;

        //单击事件的JS代码------------------------------------------------------------------------
        sClick    := '';
        if joHint.Exists('onclick') then begin
            sClick := 'dwexecute('''+dwProcQuotation(joHint.onclick)+''');';
        end;
        //
        if sClick='' then begin
            if Assigned(OnClick) then begin
                 sClick    := Format(_DWEVENT,['click',Name,'0','onclick',TForm(Owner).Handle]);
            end else begin

            end;
        end else begin
            if Assigned(OnClick) then begin
                 sClick    := Format(_DWEVENTPlus,['click',sClick,Name,'0','onclick',TForm(Owner).Handle])
            end else begin
                 sClick    := ' @click="'+sClick+'"';
            end;
        end;

        //右侧图标字符串
        sRIcon    := '';
        if joHint.Exists('righticon') then begin
            sRIcon  := '<i :class="'+dwPrefix(Actrl)+Name+'__rin"></i>'
        end;

        //
        sCode   := '<el-button'
                +' id="'+dwPrefix(Actrl)+Name+'"'
                //+sSize
                +dwVisible(TControl(ACtrl))
                +dwDisable(TControl(ACtrl))
                //+dwGetHintValue(joHint,'type','type',' type="default"')         //sButtonType
                +' :type="'+dwPrefix(Actrl)+Name+'__typ"'
                +' :icon="'+dwPrefix(Actrl)+Name+'__icn"'                                              //dwGetHintValue(joHint,'icon','icon','')         //ButtonIcon
                +dwGetHintValue(joHint,'style','','')             //样式，空（默认）/plain/round/circle
                +dwGetDWAttr(joHint)
                //动态Style
                +' :style="{'
                    +dwFontStyle(TControl(ACtrl))
                    +'left:'+dwPrefix(Actrl)+Name+'__lef,'
                    +'top:'+dwPrefix(Actrl)+Name+'__top,'
                    +'width:'+dwPrefix(Actrl)+Name+'__wid,'
                    +'height:'+dwPrefix(Actrl)+Name+'__hei'
                +'}"'
                //静态style
                +' style="position:absolute;'
                    +dwGetHintStyle(joHint,'radius','border-radius','')   //border-radius
                    +dwGetHintStyle(joHint,'backgroundcolor','background-color','')       //自定义背景色
                    //+_GetFont(Font)                   //字体
                    +dwGetDWStyle(joHint)
                +'"' //style 封闭
                +sClick
                +sEnter
                +sExit
                //+dwIIF(Assigned(OnClick),Format(_DWEVENT,['click',Name,'0','onclick',TForm(Owner).Handle]),'')
                //+dwIIF(Assigned(OnEnter),Format(_DWEVENT,['mouseenter.native',Name,'0','onenter',TForm(Owner).Handle]),'')
                //+dwIIF(Assigned(OnExit),Format(_DWEVENT,['mouseleave.native',Name,'0','onexit',TForm(Owner).Handle]),'')
                +'>{{'+dwPrefix(Actrl)+Name+'__cap}}'
                //增加右侧图标
                +'<i :class="'+dwPrefix(Actrl)+Name+'__rin"></i>'
                ;

    end;
    joRes.Add(sCode);
    Result    := (joRes);
    //
    //@mouseenter.native=“enter”
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):String;StdCall;
var
     joRes     : Variant;
begin

     //生成返回值数组
     joRes    := _Json('[]');
     //生成返回值数组
     joRes.Add('</el-button>');
     //
     Result    := (joRes);
end;

//取得Data消息
function dwGetData(ACtrl:TComponent):String;StdCall;
var
    joRes   : Variant;
    joHint  : Variant;
begin
    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    //生成返回值数组
    joRes    := _Json('[]');
    //
    with TButton(ACtrl) do begin
        joRes.Add(dwPrefix(Actrl)+Name+'__lef:"'+IntToStr(Left)+'px",');
        joRes.Add(dwPrefix(Actrl)+Name+'__top:"'+IntToStr(Top)+'px",');
        joRes.Add(dwPrefix(Actrl)+Name+'__wid:"'+IntToStr(Width)+'px",');
        joRes.Add(dwPrefix(Actrl)+Name+'__hei:"'+IntToStr(Height)+'px",');
        //
        joRes.Add(dwPrefix(Actrl)+Name+'__vis:'+dwIIF(Visible,'true,','false,'));
        joRes.Add(dwPrefix(Actrl)+Name+'__dis:'+dwIIF(Enabled,'false,','true,'));
        //
        joRes.Add(dwPrefix(Actrl)+Name+'__cap:"'+dwProcessCaption(Caption)+'",');
        //
        joRes.Add(dwPrefix(Actrl)+Name+'__typ:"'+dwGetProp(TButton(ACtrl),'type')+'",');
        //图标
        if joHint.Exists('icon') then begin
            joRes.Add(dwPrefix(Actrl)+Name+'__icn:"'+joHint.icon+'",');
        end else begin
            joRes.Add(dwPrefix(Actrl)+Name+'__icn:"",');
        end;
        if joHint.Exists('righticon') then begin
            joRes.Add(dwPrefix(Actrl)+Name+'__rin:"'+joHint.righticon+'",');
        end else begin
            joRes.Add(dwPrefix(Actrl)+Name+'__rin:"",');
        end;
        //字体
        joRes.Add(dwPrefix(Actrl)+Name+'__fcl:"'+dwColor(Font.Color)+'",');
        joRes.Add(dwPrefix(Actrl)+Name+'__fsz:"'+IntToStr(Font.size+3)+'px",');
        joRes.Add(dwPrefix(Actrl)+Name+'__ffm:"'+Font.Name+'",');
        joRes.Add(dwPrefix(Actrl)+Name+'__fwg:"'+_GetFontWeight(Font)+'",');
        joRes.Add(dwPrefix(Actrl)+Name+'__fsl:"'+_GetFontStyle(Font)+'",');
        joRes.Add(dwPrefix(Actrl)+Name+'__ftd:"'+_GetTextDecoration(Font)+'",');

    end;
    //
    Result    := (joRes);
end;

//取得事件
function dwGetAction(ACtrl:TComponent):String;StdCall;
var
    joRes     : Variant;
    joHint  : Variant;
begin
    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    //生成返回值数组
    joRes    := _Json('[]');
    //
    with TButton(ACtrl) do begin
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__lef="'+IntToStr(Left)+'px";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__top="'+IntToStr(Top)+'px";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__wid="'+IntToStr(Width)+'px";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__hei="'+IntToStr(Height+2)+'px";');
        //
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__vis='+dwIIF(Visible,'true;','false;'));
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__dis='+dwIIF(Enabled,'false;','true;'));
        //
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__cap="'+dwProcessCaption(Caption)+'";');
        //
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__typ="'+dwGetProp(TButton(ACtrl),'type')+'";');
        //图标
        if joHint.Exists('icon') then begin
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__icn="'+joHint.icon+'";');
        end else begin
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__icn="";');
        end;
        if joHint.Exists('righticon') then begin
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__rin="'+joHint.righticon+'";');
        end else begin
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__rin="";');
        end;
        //字体
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__fcl="'+dwColor(Font.Color)+'";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__fsz="'+IntToStr(Font.size+3)+'px";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__ffm="'+Font.Name+'";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__fwg="'+_GetFontWeight(Font)+'";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__fsl="'+_GetFontStyle(Font)+'";');
        joRes.Add('this.'+dwPrefix(Actrl)+Name+'__ftd="'+_GetTextDecoration(Font)+'";');
    end;
    //
    Result    := (joRes);
end;

exports
     //dwGetExtra,
     dwGetEvent,
     dwGetHead,
     dwGetTail,
     dwGetAction,
     dwGetData;
     
begin
end.
 
