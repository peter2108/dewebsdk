﻿library dwTPanel__iframe;

uses
     ShareMem,

     //
     dwCtrlBase,

     //
     SynCommons,

     //
     Messages, SysUtils, Variants, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, ExtCtrls,
     StdCtrls, Windows;



//当前控件需要引入的第三方JS/CSS
function dwGetExtra(ACtrl:TComponent):string;stdCall;
begin
     Result    := '[]';
end;



//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):string;StdCall;
var
     joData    : Variant;
begin
     with TPanel(ACtrl) do begin
     end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):string;StdCall;
var
     sCode     : string;
     joHint    : Variant;
     joRes     : Variant;
     sEnter    : String;
     sExit     : String;
     sClick    : string;
begin
    with TPanel(ACtrl) do begin

        //生成返回值数组
        joRes    := _Json('[]');

        //取得HINT对象JSON
        joHint    := dwGetHintJson(TControl(ACtrl));


        //
        sCode     := '<iframe'
                 +' id="'+dwPrefix(Actrl)+Name+'"'
                 +dwVisible(TControl(ACtrl))
                 +dwDisable(TControl(ACtrl))
                 +' :src="'+dwPrefix(Actrl)+Name+'__src"'
                 +' :frameborder="'+dwPrefix(Actrl)+Name+'__fbd"'
                 +dwGetDWAttr(joHint)
                 +' :style="{'
                 +'backgroundColor:'+dwPrefix(Actrl)+Name+'__col,'
                 +'left:'+dwPrefix(Actrl)+Name+'__lef,top:'+dwPrefix(Actrl)+Name+'__top,width:'+dwPrefix(Actrl)+Name+'__wid,height:'+dwPrefix(Actrl)+Name+'__hei}"'
                 +' style="position:absolute;overflow:hidden;'
                 +dwGetDWStyle(joHint)
                 +'"' //style 封闭
                 +'>';
        //添加到返回值数据
        joRes.Add(sCode);
        //
        Result    := (joRes);
    end;
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):string;StdCall;
var
     joRes     : Variant;
     sCode     : String;
begin
    with TPanel(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');
        //生成返回值数组
        joRes.Add('</iframe>');
        //
        Result    := (joRes);
    end;
end;

//取得Data
function dwGetData(ACtrl:TComponent):string;StdCall;
var
    joRes     : Variant;
    joHint    : Variant;
begin
    with TPanel(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');

        //取得HINT对象JSON
        joHint    := dwGetHintJson(TControl(ACtrl));

        //
        with TPanel(ACtrl) do begin
            joRes.Add(dwPrefix(Actrl)+Name+'__lef:"'+IntToStr(Left)+'px",');
            joRes.Add(dwPrefix(Actrl)+Name+'__top:"'+IntToStr(Top)+'px",');
            joRes.Add(dwPrefix(Actrl)+Name+'__wid:"'+IntToStr(Width)+'px",');
            joRes.Add(dwPrefix(Actrl)+Name+'__hei:"'+IntToStr(Height)+'px",');
            //
            joRes.Add(dwPrefix(Actrl)+Name+'__vis:'+dwIIF(Visible,'true,','false,'));
            joRes.Add(dwPrefix(Actrl)+Name+'__dis:'+dwIIF(Enabled,'false,','true,'));
            //
            joRes.Add(dwPrefix(Actrl)+Name+'__src:"'+joHint.src+'",');
            joRes.Add(dwPrefix(Actrl)+Name+'__fbd:"'+Integer(BorderStyle).ToString+'",');
            //
            if TPanel(ACtrl).Color = clNone then begin
                joRes.Add(dwPrefix(Actrl)+Name+'__col:"rgba(0,0,0,0)",');
            end else begin
                joRes.Add(dwPrefix(Actrl)+Name+'__col:"'+dwAlphaColor(TPanel(ACtrl))+'",');
            end;
        end;
        //
        Result    := (joRes);
    end;
end;

function dwGetAction(ACtrl:TComponent):string;StdCall;
var
    joRes   : Variant;
    joHint  : Variant;
begin
    with TPanel(ACtrl) do begin
        //生成返回值数组
        joRes    := _Json('[]');

        //取得HINT对象JSON
        joHint    := dwGetHintJson(TControl(ACtrl));

        //
        with TPanel(ACtrl) do begin
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__lef="'+IntToStr(Left)+'px";');
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__top="'+IntToStr(Top)+'px";');
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__wid="'+IntToStr(Width)+'px";');
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__hei="'+IntToStr(Height)+'px";');
            //
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__vis='+dwIIF(Visible,'true;','false;'));
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__dis='+dwIIF(Enabled,'false;','true;'));
            //
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__src="'+joHint.src+'";');
            joRes.Add('this.'+dwPrefix(Actrl)+Name+'__fbd="'+Integer(BorderStyle).ToString+'";');
            //
            if TPanel(ACtrl).Color = clNone then begin
                joRes.Add('this.'+dwPrefix(Actrl)+Name+'__col="rgba(0,0,0,0)";');
            end else begin
                joRes.Add('this.'+dwPrefix(Actrl)+Name+'__col="'+dwAlphaColor(TPanel(ACtrl))+'";');
            end;
        end;

        //
        Result    := (joRes);
    end;
end;


exports
    dwGetExtra,
    dwGetEvent,
    dwGetHead,
    dwGetTail,
    dwGetAction,
    dwGetData;

begin
end.
 
