object Form2: TForm2
  Left = 0
  Top = 20
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'DeWeb Check in'
  ClientHeight = 408
  ClientWidth = 304
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 20
  object Label2: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 3
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    Alignment = taCenter
    AutoSize = False
    Caption = 'User'
    Layout = tlCenter
    ExplicitLeft = 24
    ExplicitTop = 0
    ExplicitWidth = 100
  end
  object Label3: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 81
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    Caption = 'Password'
    Layout = tlCenter
    ExplicitLeft = 24
    ExplicitTop = 78
    ExplicitWidth = 100
  end
  object Label4: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 159
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    Caption = 'Memo'
    Layout = tlCenter
    ExplicitLeft = 24
    ExplicitWidth = 100
  end
  object Label1: TLabel
    AlignWithMargins = True
    Left = 10
    Top = 237
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    Caption = 'Type'
    Layout = tlCenter
    ExplicitLeft = 24
    ExplicitTop = 239
    ExplicitWidth = 100
  end
  object Button1: TButton
    AlignWithMargins = True
    Left = 10
    Top = 352
    Width = 284
    Height = 41
    Hint = '{"type":"success"}'
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    Caption = 'Sign in'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    AlignWithMargins = True
    Left = 10
    Top = 42
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    TabOrder = 1
    Text = 'admin'
  end
  object Edit2: TEdit
    AlignWithMargins = True
    Left = 10
    Top = 120
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    PasswordChar = '*'
    TabOrder = 2
    Text = '12345'
  end
  object Edit3: TEdit
    AlignWithMargins = True
    Left = 10
    Top = 198
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    AutoSize = False
    TabOrder = 3
  end
  object ComboBox1: TComboBox
    AlignWithMargins = True
    Left = 10
    Top = 276
    Width = 284
    Height = 33
    Margins.Left = 10
    Margins.Right = 10
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ItemIndex = 0
    ParentFont = False
    TabOrder = 4
    Text = #21271#20140
    Items.Strings = (
      #21271#20140
      #19978#28023
      #28145#22323
      #35199#23433
      #35140#38451)
  end
  object CheckBox1: TCheckBox
    AlignWithMargins = True
    Left = 10
    Top = 322
    Width = 284
    Height = 17
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 10
    Align = alTop
    Caption = 'Remeber me'
    TabOrder = 5
    OnClick = CheckBox1Click
  end
end
