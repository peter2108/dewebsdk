object Form_Inventory: TForm_Inventory
  Left = 0
  Top = 0
  HelpKeyword = 'embed'
  HelpContext = 3344
  Caption = 'Form_Inventory'
  ClientHeight = 619
  ClientWidth = 883
  Color = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 883
    Height = 53
    Hint = '{"dwstyle":"border-bottom:solid 1px #ed6d00;"}'
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object Edit_Search: TEdit
      AlignWithMargins = True
      Left = 10
      Top = 11
      Width = 503
      Height = 30
      Hint = 
        '{"placeholder":"'#35831#36755#20837#26597#35810#20851#38190#23383','#25903#25345#22810#20851#38190#23383#25628#32034'","radius":"15px","suffix-icon"' +
        ':"el-icon-search","dwstyle":"padding-left:10px;"}'
      Margins.Left = 10
      Margins.Top = 11
      Margins.Right = 1
      Margins.Bottom = 12
      Align = alLeft
      TabOrder = 0
      OnKeyUp = Edit_SearchKeyUp
      ExplicitHeight = 28
    end
    object Button_Search2: TButton
      AlignWithMargins = True
      Left = 515
      Top = 11
      Width = 78
      Height = 32
      Hint = 
        '{"type":"primary","icon":"el-icon-search","radius":"0 3px 3px 0"' +
        '}'
      Margins.Left = 1
      Margins.Top = 11
      Margins.Bottom = 10
      Align = alLeft
      Caption = #26597#35810
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      OnClick = Button_Search2Click
    end
    object Button_Show: TButton
      AlignWithMargins = True
      Left = 597
      Top = 11
      Width = 163
      Height = 32
      Hint = '{"type":"success"}'
      Margins.Left = 1
      Margins.Top = 11
      Margins.Bottom = 10
      Align = alLeft
      Caption = #24377#20986#24335#31383#20307
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button_ShowClick
      ExplicitLeft = 614
      ExplicitTop = 10
      ExplicitHeight = 30
    end
  end
  object StringGrid1: TStringGrid
    AlignWithMargins = True
    Left = 10
    Top = 63
    Width = 863
    Height = 330
    Hint = '{"headerbackground":"#F7F7F7","dwattr":"stripe"}'
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 0
    Align = alTop
    ColCount = 8
    DefaultColWidth = 150
    DefaultRowHeight = 30
    RowCount = 11
    TabOrder = 1
    OnGetEditMask = StringGrid1GetEditMask
    ColWidths = (
      150
      150
      150
      150
      150
      150
      150
      150)
  end
  object TrackBar1: TTrackBar
    AlignWithMargins = True
    Left = 10
    Top = 403
    Width = 863
    Height = 30
    HelpType = htKeyword
    HelpKeyword = 'page'
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 20
    Align = alTop
    TabOrder = 2
    OnChange = TrackBar1Change
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 144
    Top = 136
  end
end
