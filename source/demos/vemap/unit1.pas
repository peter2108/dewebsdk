﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    Math,SysUtils,
    Winapi.Windows, System.Classes, Vcl.Controls,Forms, Vcl.Grids, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    SG: TStringGrid;
    Label2: TLabel;
    Panel_0_Title: TPanel;
    Label3: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
begin
    Randomize;
    for var iRow := 1 to SG.RowCount-1 do begin
        for var iCol := 1 to SG.ColCount-1 do begin
            SG.Cells[iCol,iRow] := FloatToStr(StrToFloat(SG.Cells[iCol,iRow])+Random(100)/10);
        end;
    end;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    var I := 0;
    SG.ColCount     := 4;
    SG.RowCount     := 37;

    SG.Cells[0,I]   := '位置'; SG.Cells[1,I]   := '税收'; SG.Cells[2,I]   := '人口'; SG.Cells[3,I]   := '面积'; Inc(I);
    SG.Cells[0,I]   := '河北'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '湖北'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '上海'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '山西'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '辽宁'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '吉林'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '黑龙江'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '江苏'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '浙江'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '安徽'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '福建'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '江西'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '山东'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '河南'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '湖北'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '湖南'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '广东'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '海南'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '四川'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '贵州'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '云南'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '陕西'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '甘肃'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '青海'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '台湾'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '内蒙古'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '广西'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '西藏'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '宁夏'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '新疆'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '北京'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '天津'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '上海'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '5493.71'; Inc(I);
    SG.Cells[0,I]   := '重庆'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '9411.85'; Inc(I);
    SG.Cells[0,I]   := '香港'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '2454.37'; Inc(I);
    SG.Cells[0,I]   := '澳门'; SG.Cells[1,I]   := '581.55'; SG.Cells[2,I]   := '583.92'; SG.Cells[3,I]   := '3519.03';

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //
    if (X>600)and(Y>600) then begin
        dwSetPCMode(self);
        Width   := 600;
    end else begin
        dwSetMobileMode(self,414,736);
        Height  := Max(400,Height);
    end;
end;

end.
