﻿unit unit1;
interface
uses
    //
    dwBase,
    //
    DateUtils,
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Grids,
    Vcl.Menus, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, Vcl.Imaging.jpeg,
    Vcl.Buttons,Winapi.ShellAPI;
type
  TForm1 = class(TForm)
    Label1: TLabel;
    Image2: TImage;
    Image1: TImage;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1EndDock(Sender, Target: TObject; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    gsMainDir: string;
     function DoCopyFile(const vCopyFileName: string; vNewFileName: string): Boolean;
  end;
var
  Form1: TForm1;
implementation
{$R *.dfm}

procedure TForm1.btn1EndDock(Sender, Target: TObject; X, Y: Integer);
var
sLast: string;
begin
sLast := dwGetProp(TForm(self.Parent), '__upload');
if sLast <> '' then
 if DoCopyFile(gsMainDir + 'upload\' + sLast, gsMainDir + 'media\images\uploadimg\' + sLast) then
              image1.Hint := '{"radius":"5px","src":"media/images/uploadimg/' + sLast + '"}';

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    Image1.Hint := '{"radius":"10px","src":"media/images/mn/3.jpg?time='+IntToStr((DateTimeToUnix(Now)-86060)*1000)+'"}';

end;

function TForm1.DoCopyFile(const vCopyFileName: string;
  vNewFileName: string): Boolean;
var
  lMemory: TMemoryStream;
begin
  Result := false;
  try
    //if not CopyFile(PWideChar(vCopyFileName), PWideChar(vNewFileName), false) then
    //MOVEFILE_REPLACE_EXISTING 如果指定位置有同名文件，则覆盖之
    if not MoveFileEx(PWideChar(vCopyFileName), PWideChar(vNewFileName), MOVEFILE_REPLACE_EXISTING) then
    begin
      //采用流写入文件 ,360等软件禁止COPY文件,采用流读写保存方式
      lMemory := TMemoryStream.Create();
      try
        lMemory.LoadFromFile(vCopyFileName);
        lMemory.Position := 0;
        lMemory.SaveToFile(vNewFileName);
      finally
        lMemory.Free;
      end;
    end;
  except
  end;
  if not FileExists(vNewFileName) then
  begin
    dwShowMessage('上传出错！', Self);
    exit;
  end;
  Result := True;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
gsMainDir := ExtractFilePath(Application.ExeName);
end;
end.
