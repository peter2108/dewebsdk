﻿unit unit1;

interface

uses
    //
    dwBase,
    dwSGUnit,
    HzUnit,

    //
    CloneComponents,
    SynCommons,

    //
    Math,
    Graphics,
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.Buttons, Data.DB, Vcl.DBGrids, Vcl.ComCtrls,
  Data.Win.ADODB, Vcl.DBCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    SG: TStringGrid;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
    gsKeys  : array[0..8] of string;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
begin
    dwSGSetRow(SG, 1,clBlue,clWhite);

end;

procedure TForm1.Edit1Change(Sender: TObject);
var
    iR      : Integer;
    sKey    : String;
begin
    for iR := 1 to SG.RowCount-1 do begin
        sKey    := LowerCase(Edit1.Text);
        if (Pos(sKey,SG.Cells[0,iR]) > 0) or (Pos(sKey,SG.Cells[1,iR]) > 0) or (Pos(sKey,gsKeys[iR-1]) > 0) then begin
            SG.Row  := iR;
            break;
        end;
    end;

end;

procedure TForm1.Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
    iR      : Integer;
    sKey    : String;
begin
    //
    case Key of
        VK_UP : begin   //'上箭头';
            //dwSGSetRow(SG, 1,clBlue,clWhite);
            if SG.Row < 2 then begin
                SG.Row  := 1;
            end else begin
                SG.Row  := SG.Row - 1;
            end;
        end;
        VK_DOWN : begin//'下箭头';
            if SG.Row > 8 then begin
                SG.Row  := 9;
            end else begin
                SG.Row  := SG.Row + 1;
            end;
        end;
        13 : begin
            Edit1.Text  := SG.Cells[0,SG.Row]+' / '+SG.Cells[1,SG.Row];
        end;
    else
    end;
end;

procedure TForm1.FormShow(Sender: TObject);
var
    iR,iC   : Integer;
begin
     //
     with SG do begin
          Cells[0,0]   := '姓名';
          Cells[1,0]   := '详细地址';
          //
          Cells[0,1]   := '张云政';
          Cells[1,1]   := '电力局办公室01001号';
          //
          Cells[0,2]   := '李景学';
          Cells[1,2]   := '烟草公司驻外楼2901室';
          //
          Cells[0,3]   := '周子琴';
          Cells[1,3]   := '外滩管理事务局管理处';
          //
          Cells[0,4]   := '谢玲芳';
          Cells[1,4]   := '民航管理中心监察室9901';
          //
          Cells[0,5]   := '张曼玉';
          Cells[1,5]   := '无线集团';
          //
          Cells[0,6]   := '刘德华';
          Cells[1,6]   := '房地产公司驻外楼1212室';
          //
          Cells[0,7]   := '梁朝伟';
          Cells[1,7]   := '数据中心';
          //
          Cells[0,8]   := '张益';
          Cells[1,8]   := '金融管理大队';
          //
          Cells[0,9]   := '贾玲';
          Cells[1,9]   := '综合演艺管理中心';
          //
          ColWidths[0]   := 100;
          ColWidths[1]   := SG.Width-101;
     end;

     //
     for iR := 1 to SG.RowCount-1 do begin
        gsKeys[iR-1]    := LowerCase(HzToPy(SG.Cells[0,iR]+'_'+SG.Cells[1,iR]));
     end;
     //以下显示默认选中,SG为StringGrid控件
     dwRunJS('this.$refs.'+dwPrefix(SG)+SG.Name+'.setCurrentRow(this.$refs.'+dwPrefix(SG)+SG.Name+'.data[0]);',self);
end;

end.
