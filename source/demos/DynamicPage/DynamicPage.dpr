﻿library DynamicPage;

uses
  ShareMem,
  SysUtils,
  Forms,
  Messages,
  StdCtrls,
  Variants,
  Windows,
  Classes,
  Data.Win.ADODB,
  unit1 in 'unit1.pas' {Form1},
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas' {Form3},
  Unit4 in 'Unit4.pas' {Form4};

{$R *.res}

var
     DLLApp         : TApplication;
     DLLScreen      : TScreen;


function dwLoad(AParams:String;AConnection:TADOConnection;AApp:TApplication;AScreen:TScreen):TForm;stdcall;
var
    sDir        : string;
begin
    //
    Application := AApp;
    Screen      := AScreen;
    //

    //
    Form1       := TForm1.Create(nil);
    Form1.gsMainDir := ExtractFilePath(Application.ExeName);

    //Form1.ADOQuery1.Connection   := AConnection;
    //----------库存查询----------------------------------------------------------------------------
    //创建FORM
    Form1.Form2   := TForm2.Create(Form1);
    //嵌入到TabSheet中
    Form1.Form2.Parent  := Form1.TabSheet2;
    //设置嵌入标识,必须
    Form1.Form2.HelpKeyword := 'embed';


    Result      := Form1;
end;

procedure DLLUnloadProc(dwReason: DWORD);
begin
     if dwReason = DLL_PROCESS_DETACH then begin
          Application    := DLLApp; //恢复
          Screen         := DLLScreen;
     end;
end;



exports
     dwLoad;

begin
     DLLApp    := Application;     //保存 DLL 中初始的 Application 对象
     DLLScreen := Screen;
     DLLProc   := @DLLUnloadProc;  //保证 DLL 卸载时恢复原来的 Application
     DLLUnloadProc(DLL_PROCESS_DETACH);
end.
