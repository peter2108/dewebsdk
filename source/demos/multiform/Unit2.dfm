object Form2: TForm2
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Form2'
  ClientHeight = 559
  ClientWidth = 1093
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 1087
    Height = 553
    Align = alClient
    Caption = 'Panel1'
    Color = clSkyBlue
    ParentBackground = False
    TabOrder = 0
    object StringGrid1: TStringGrid
      Left = 24
      Top = 47
      Width = 345
      Height = 290
      RowCount = 10
      TabOrder = 0
      RowHeights = (
        24
        24
        24
        24
        24
        24
        24
        24
        24
        24)
    end
    object Button1: TButton
      Left = 24
      Top = 8
      Width = 75
      Height = 33
      Caption = 'Button1'
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object StringGrid2: TStringGrid
    Left = 432
    Top = 50
    Width = 345
    Height = 290
    RowCount = 10
    TabOrder = 1
  end
end
