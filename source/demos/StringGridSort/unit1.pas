﻿unit unit1;

interface

uses
     //
     dwBase,

     //
     Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
     Vcl.Controls, Vcl.Forms, Vcl.StdCtrls, Vcl.Grids, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Menus,
  Vcl.Buttons, Data.DB, Data.Win.ADODB, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    StringGrid1: TStringGrid;
    procedure FormCreate(Sender: TObject);
    procedure StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
    procedure StringGrid1Click(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormCreate(Sender: TObject);
var
     iR,iC     : Integer;
begin

     //
     Top  := 0;
     //
     with StringGrid1 do begin
          Cells[0,0]   := '{"caption":"日期","type":"selection","sort":true}';
          Cells[1,0]   := '{"caption":"姓名","align":"center","sort":true}';
          Cells[2,0]   := '{"caption":"年龄","align":"center","sort":true,"filter":["18","19","47"]}';
          Cells[3,0]   := '{"caption":"城市","align":"center","sort":true,"filter":["襄阳","朝阳区","阳"]}';
          Cells[4,0]   := '详细地址';
          //
          Cells[0,1]   := '2000-07-01';
          Cells[1,1]   := '张云政';
          Cells[2,1]   := '47';
          Cells[3,1]   := '襄阳';
          Cells[4,1]   := '电力局办公室01001号';
          //
          Cells[0,2]   := '2001-07-01';
          Cells[1,2]   := '李景学';
          Cells[2,2]   := '45';
          Cells[3,2]   := '朝阳区';
          Cells[4,2]   := '烟草公司驻外楼2901室';
          //
          Cells[0,3]   := '2002-12-01';
          Cells[1,3]   := '周子琴';
          Cells[2,3]   := '32';
          Cells[3,3]   := '淞沪区';
          Cells[4,3]   := '外滩管理事务局管理处';
          //
          Cells[0,4]   := '2006-03-01';
          Cells[1,4]   := '谢玲芳';
          Cells[2,4]   := '25';
          Cells[3,4]   := '沈阳';
          Cells[4,4]   := '民航管理中心监察室9901';
          //
          Cells[0,5]   := '2007-03-01';
          Cells[1,5]   := '张曼玉';
          Cells[2,5]   := '218';
          Cells[3,5]   := '九龙';
          Cells[4,5]   := '无线集团';
          //
          Cells[0,6]   := '2008-03-01';
          Cells[1,6]   := '刘德华';
          Cells[2,6]   := '43';
          Cells[3,6]   := '兰州';
          Cells[4,6]   := '房地产公司驻外楼1212室';
          //
          Cells[0,7]   := '2009-03-01';
          Cells[1,7]   := '梁朝伟';
          Cells[2,7]   := '22';
          Cells[3,7]   := '济南';
          Cells[4,7]   := '数据中心';
          //
          Cells[0,8]   := '2010-03-01';
          Cells[1,8]   := '张一山';
          Cells[2,8]   := '26';
          Cells[3,8]   := '大连';
          Cells[4,8]   := '金融管理大队';
          //
          Cells[0,9]   := '2011-03-01';
          Cells[1,9]   := '柳小岩';
          Cells[2,9]   := '27';
          Cells[3,9]   := '宜城';
          Cells[4,9]   := '综合演艺管理中心';
          //
          Cells[0,10]   := '2001-07-01';
          Cells[1,10]   := '张景学';
          Cells[2,10]   := '28';
          Cells[3,10]   := '朝阳区';
          Cells[4,10]   := '烟草公司驻外楼2901室';
          //
          Cells[0,11]   := '2000-01-01';
          Cells[1,11]   := '李云龙';
          Cells[2,11]   := '29';
          Cells[3,11]   := '襄阳';
          Cells[4,11]   := '电力局办公室01001号';
          //
          Cells[0,12]   := '2011-07-01';
          Cells[1,12]   := '马小云';
          Cells[2,12]   := '30';
          Cells[3,12]   := '朝阳区';
          Cells[4,12]   := '烟草公司驻外楼2901室';
          //
          Cells[0,13]   := '2012-12-01';
          Cells[1,13]   := '邹子琴';
          Cells[2,13]   := '18';
          Cells[3,13]   := '淞沪区';
          Cells[4,13]   := '外滩管理事务局管理处';
          //
          Cells[0,14]   := '2016-03-01';
          Cells[1,14]   := '谢文芳';
          Cells[2,14]   := '119';
          Cells[3,14]   := '沈阳';
          Cells[4,14]   := '民航管理中心监察室9901';
          //
          Cells[0,15]   := '2017-03-01';
          Cells[1,15]   := '林曼玉';
          Cells[2,15]   := '50';
          Cells[3,15]   := '九龙';
          Cells[4,15]   := '无线集团';
          //
          Cells[0,16]   := '2018-03-01';
          Cells[1,16]   := '马德华';
          Cells[2,16]   := '51';
          Cells[3,16]   := '兰州';
          Cells[4,16]   := '房地产公司驻外楼1212室';
          //
          Cells[0,17]   := '2019-03-01';
          Cells[1,17]   := '梁二伟';
          Cells[2,17]   := '52';
          Cells[3,17]   := '济南';
          Cells[4,17]   := '数据中心';
          //
          Cells[0,18]   := '2020-03-01';
          Cells[1,18]   := '张小益';
          Cells[2,18]   := '53';
          Cells[3,18]   := '大连';
          Cells[4,18]   := '金融管理大队';
          //
          Cells[0,19]   := '2021-03-01';
          Cells[1,19]   := '贾小玲';
          Cells[2,19]   := '132';
          Cells[3,19]   := '宜城';
          Cells[4,19]   := '综合管理中心';
          //
          ColWidths[0]   := 140;
          ColWidths[1]   := 120;
          ColWidths[2]   := 120;
          ColWidths[3]   := 120;
          ColWidths[4]   := 200;
     end;
end;



procedure TForm1.StringGrid1Click(Sender: TObject);
begin
    Caption := StringGrid1.Row.ToString;
end;

procedure TForm1.StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
begin
    if Value='sort' then begin
        //用作排序时 ACol为列序号, ARow为1时升序,0时降序, Value='sort'
        if ACol = 2 then begin
            //根据数字排序
            dwGridQuickSort(StringGrid1,ACol,ARow=1,True);
        end else begin
            //字符串排序
            dwGridQuickSort(StringGrid1,ACol,ARow=1,False);
        end;
    end else if Copy(Value,1,9)='"filter":' then begin
        //
        dwGridQuickFilter(StringGrid1,ACol,'{'+Value+'}');
    end;

end;

end.
