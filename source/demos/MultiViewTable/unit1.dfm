﻿object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  AutoSize = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 2050
  ClientWidth = 1200
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clAqua
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 21
  object Panel_1_Content: TPanel
    Left = 0
    Top = 50
    Width = 1200
    Height = 2000
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Panel_1_Content'
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    ExplicitWidth = 1300
    object TrackBar_Page: TTrackBar
      AlignWithMargins = True
      Left = 3
      Top = 108
      Width = 1194
      Height = 30
      HelpType = htKeyword
      HelpKeyword = 'page'
      Margins.Top = 10
      Margins.Bottom = 20
      Align = alTop
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 171
      ExplicitWidth = 1294
    end
    object Panel5: TPanel
      AlignWithMargins = True
      Left = 0
      Top = 0
      Width = 1200
      Height = 41
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 1
      ExplicitTop = 8
      ExplicitWidth = 1300
      object Panel6: TPanel
        Left = 96
        Top = 0
        Width = 100
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Width = 86
          Height = 27
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #22995#21517
          Layout = tlCenter
          ExplicitWidth = 32
          ExplicitHeight = 21
        end
      end
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 96
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object CheckBox2: TCheckBox
          Left = 11
          Top = 9
          Width = 30
          Height = 17
          TabOrder = 0
        end
      end
      object Panel11: TPanel
        Left = 196
        Top = 0
        Width = 45
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
        object 性别: TLabel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Width = 31
          Height = 27
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #24615#21035
          Layout = tlCenter
          ExplicitWidth = 32
          ExplicitHeight = 21
        end
      end
      object Panel12: TPanel
        Left = 241
        Top = 0
        Width = 144
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 3
        object Label4: TLabel
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 134
          Height = 31
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = #32844#21153
          Color = clAppWorkSpace
          ParentColor = False
          Layout = tlCenter
          WordWrap = True
          ExplicitLeft = 96
          ExplicitTop = -180
          ExplicitWidth = 236
          ExplicitHeight = 217
        end
      end
      object Panel13: TPanel
        Left = 385
        Top = 0
        Width = 127
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 4
        ExplicitLeft = 708
        object Label_Province: TLabel
          Left = 0
          Top = 0
          Width = 123
          Height = 37
          Align = alClient
          Alignment = taCenter
          AutoSize = False
          Caption = #30465#20221#65306
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          Layout = tlCenter
          ExplicitLeft = 30
          ExplicitTop = 13
          ExplicitWidth = 45
          ExplicitHeight = 20
        end
      end
      object Panel15: TPanel
        Left = 512
        Top = 0
        Width = 140
        Height = 41
        Hint = 
          '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
          'x #DCDFE6;border-top:solid 1px #DCDFE6;"}'
        Align = alLeft
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 5
        ExplicitLeft = 835
        object Label6: TLabel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Width = 126
          Height = 27
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #24037#36164
          Layout = tlCenter
          ExplicitWidth = 32
          ExplicitHeight = 21
        end
      end
      object Panel16: TPanel
        Left = 652
        Top = 0
        Width = 548
        Height = 41
        Hint = '{"dwstyle":"border:solid 1px #DCDFE6;"}'
        Align = alClient
        BevelKind = bkTile
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 6
        ExplicitLeft = 975
        ExplicitWidth = 325
        object Label7: TLabel
          AlignWithMargins = True
          Left = 5
          Top = 5
          Width = 534
          Height = 27
          Margins.Left = 5
          Margins.Top = 5
          Margins.Right = 5
          Margins.Bottom = 5
          Align = alClient
          Alignment = taCenter
          Caption = #25805#20316
          Layout = tlCenter
          ExplicitWidth = 32
          ExplicitHeight = 21
        end
      end
    end
    object Panel_Records: TPanel
      Left = 0
      Top = 41
      Width = 1200
      Height = 57
      Align = alTop
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 2
      object Panel_Record: TPanel
        AlignWithMargins = True
        Left = 0
        Top = 0
        Width = 1200
        Height = 57
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        ParentBackground = False
        TabOrder = 0
        Visible = False
        ExplicitTop = 41
        ExplicitWidth = 1300
        object Panel8: TPanel
          Left = 96
          Top = 0
          Width = 100
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 0
          object Label_Name: TLabel
            Left = 0
            Top = 0
            Width = 96
            Height = 53
            Align = alClient
            Alignment = taCenter
            AutoSize = False
            Caption = #26446#21326#21191
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = #24494#36719#38597#40657
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
            ExplicitLeft = -1
            ExplicitTop = 4
            ExplicitWidth = 95
            ExplicitHeight = 36
          end
        end
        object Panel9: TPanel
          Left = 241
          Top = 0
          Width = 144
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 1
          object ComboBox_HeadShip: TComboBox
            Left = 4
            Top = 14
            Width = 113
            Height = 29
            Hint = '{"dwstyle":"border:0px;color:#000;"}'
            Color = clNone
            TabOrder = 0
            Text = 'ComboBox_HeadShip'
            Items.Strings = (
              #24635#32463#29702
              #21103#24635#32463#29702
              #32463#29702#21161#29702
              #20250#35745
              #21161#29702
              #20445#31649#21592
              #20986#32435
              #38144#21806#21592
              #25216#26415#37096#37096#38271
              #25216#26415#20154#21592
              #36136#26816#37096#37096#38271
              #36136#26816#21592
              #20844#20851#37096#37096#38271
              #20844#20851#21592)
          end
        end
        object Panel2: TPanel
          Left = 512
          Top = 0
          Width = 140
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 2
          object Panel_T_C_Client: TPanel
            AlignWithMargins = True
            Left = 3
            Top = 17
            Width = 133
            Height = 33
            Margins.Right = 0
            Align = alBottom
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
          end
          object SpinEdit_Salary: TSpinEdit
            Left = 4
            Top = 14
            Width = 103
            Height = 31
            Hint = '{"dwstyle":"border:0px;color:#000;"}'
            Color = clNone
            Ctl3D = True
            MaxValue = 0
            MinValue = 0
            ParentCtl3D = False
            TabOrder = 1
            Value = 0
          end
        end
        object Panel3: TPanel
          Left = 385
          Top = 0
          Width = 127
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 3
          object Edit_Province: TEdit
            Left = 4
            Top = 14
            Width = 113
            Height = 29
            Hint = '{"dwstyle":"border:0px;color:#000;"}'
            Color = clNone
            TabOrder = 0
          end
        end
        object Panel4: TPanel
          Left = 196
          Top = 0
          Width = 45
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 4
          object Label_Sex: TLabel
            Left = 0
            Top = 0
            Width = 41
            Height = 53
            Align = alClient
            Alignment = taCenter
            AutoSize = False
            Caption = #30007
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = #24494#36719#38597#40657
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
            ExplicitLeft = 88
            ExplicitTop = 33
            ExplicitWidth = 20
            ExplicitHeight = 20
          end
        end
        object Panel14: TPanel
          Left = 652
          Top = 0
          Width = 548
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;border-right:solid 1px #DCDFE6;"}'
          Align = alClient
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 5
          ExplicitWidth = 648
          object Label_Addr: TLabel
            Left = 4
            Top = 6
            Width = 405
            Height = 46
            AutoSize = False
            Caption = #22320#22336
            Font.Charset = ANSI_CHARSET
            Font.Color = 6579300
            Font.Height = -16
            Font.Name = #24494#36719#38597#40657
            Font.Style = []
            ParentFont = False
            Layout = tlCenter
          end
          object Button_ID: TButton
            AlignWithMargins = True
            Left = 415
            Top = 12
            Width = 121
            Height = 30
            Hint = '{"type":"primary"}'
            Margins.Top = 12
            Margins.Right = 8
            Margins.Bottom = 11
            Align = alRight
            Font.Charset = ANSI_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = #24494#36719#38597#40657
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            ExplicitLeft = 520
            ExplicitTop = 3
            ExplicitHeight = 47
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 96
          Height = 57
          Hint = 
            '{"dwstyle":"border-left:solid 1px #DCDFE6;border-bottom:solid 1p' +
            'x #DCDFE6;"}'
          Align = alLeft
          BevelKind = bkTile
          BevelOuter = bvNone
          Color = clWhite
          ParentBackground = False
          TabOrder = 6
          object Image_Avatar: TImage
            AlignWithMargins = True
            Left = 44
            Top = 3
            Width = 45
            Height = 47
            Align = alRight
            Stretch = True
            ExplicitLeft = 47
            ExplicitTop = 0
            ExplicitHeight = 53
          end
          object CheckBox1: TCheckBox
            Left = 11
            Top = 17
            Width = 30
            Height = 17
            TabOrder = 0
          end
        end
      end
    end
  end
  object Panel_0_Banner: TPanel
    Left = 0
    Top = 0
    Width = 1200
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = 4271650
    ParentBackground = False
    TabOrder = 1
    ExplicitWidth = 1300
    object Label_Introduce: TLabel
      AlignWithMargins = True
      Left = 210
      Top = 3
      Width = 987
      Height = 44
      Margins.Left = 10
      Align = alClient
      Caption = ' DeWeb multiview table demo'
      Color = 4210752
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
      ExplicitWidth = 260
      ExplicitHeight = 18
    end
    object Panel_Title: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 50
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold, fsItalic]
      ParentBackground = False
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object Label_Title: TLabel
        Left = 0
        Top = 0
        Width = 200
        Height = 50
        HelpType = htKeyword
        Align = alClient
        Alignment = taCenter
        Caption = 'MultiView'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Layout = tlCenter
        WordWrap = True
        ExplicitWidth = 133
        ExplicitHeight = 29
      end
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 264
    Top = 394
  end
end
