﻿unit unit1;

interface

uses
    //
    dwBase,

    //
    Math,
    System.SysUtils,
    Winapi.Windows, System.Classes, Vcl.Controls,Forms, Vcl.Grids, Vcl.StdCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    SG: TStringGrid;
    Panel_0_Title: TPanel;
    Label1: TLabel;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.Button1Click(Sender: TObject);
begin
    Randomize;
    for var iRow := 1 to SG.RowCount-1 do begin
        for var iCol := 1 to SG.ColCount-1 do begin
            SG.Cells[iCol,iRow] := FloatToStr(StrToFloat(SG.Cells[iCol,iRow])+Random(100)/10);
        end;
    end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    var I := 0;

    SG.Cells[0,I]   := '日期'; SG.Cells[1,I]   := 'Open'; SG.Cells[2,I]   := 'Close'; SG.Cells[3,I]   := 'Lowest'; SG.Cells[4,I]   := 'Highest'; SG.Cells[5,I]   := 'Vol';Inc(I);

    SG.Cells[0,I]   := '2004-01-05'; SG.Cells[1,I]   := '411.85'; SG.Cells[2,I]   := '544.07'; SG.Cells[3,I]   := '411.85'; SG.Cells[4,I]   := '575.92'; SG.Cells[5,I]   := '221290000';Inc(I);
    SG.Cells[0,I]   := '2004-01-06'; SG.Cells[1,I]   := '543.85'; SG.Cells[2,I]   := '538.66'; SG.Cells[3,I]   := '454.37'; SG.Cells[4,I]   := '584.07'; SG.Cells[5,I]   := '191460000';Inc(I);
    SG.Cells[0,I]   := '2004-01-07'; SG.Cells[1,I]   := '535.46'; SG.Cells[2,I]   := '529.03'; SG.Cells[3,I]   := '432.12'; SG.Cells[4,I]   := '587.55'; SG.Cells[5,I]   := '225490000';Inc(I);
    SG.Cells[0,I]   := '2004-01-08'; SG.Cells[1,I]   := '530.07'; SG.Cells[2,I]   := '592.44'; SG.Cells[3,I]   := '480.59'; SG.Cells[4,I]   := '651.99'; SG.Cells[5,I]   := '237770000';Inc(I);
    SG.Cells[0,I]   := '2004-01-09'; SG.Cells[1,I]   := '589.25'; SG.Cells[2,I]   := '458.89'; SG.Cells[3,I]   := '420.52'; SG.Cells[4,I]   := '603.48'; SG.Cells[5,I]   := '223250000';Inc(I);
    SG.Cells[0,I]   := '2004-01-12'; SG.Cells[1,I]   := '461.55'; SG.Cells[2,I]   := '485.18'; SG.Cells[3,I]   := '389.85'; SG.Cells[4,I]   := '543.03'; SG.Cells[5,I]   := '197960000';Inc(I);
    SG.Cells[0,I]   := '2004-01-13'; SG.Cells[1,I]   := '485.18'; SG.Cells[2,I]   := '427.18'; SG.Cells[3,I]   := '341.19'; SG.Cells[4,I]   := '539.25'; SG.Cells[5,I]   := '197310000';Inc(I);
    SG.Cells[0,I]   := '2004-01-14'; SG.Cells[1,I]   := '428.67'; SG.Cells[2,I]   := '538.37'; SG.Cells[3,I]   := '426.89'; SG.Cells[4,I]   := '573.85'; SG.Cells[5,I]   := '186280000';Inc(I);
    SG.Cells[0,I]   := '2004-01-15'; SG.Cells[1,I]   := '534.52'; SG.Cells[2,I]   := '553.85'; SG.Cells[3,I]   := '454.52'; SG.Cells[4,I]   := '639.03'; SG.Cells[5,I]   := '260090000';Inc(I);
    SG.Cells[0,I]   := '2004-01-16'; SG.Cells[1,I]   := '556.37'; SG.Cells[2,I]   := '600.51'; SG.Cells[3,I]   := '503.71'; SG.Cells[4,I]   := '666.88'; SG.Cells[5,I]   := '254170000';Inc(I);
    SG.Cells[0,I]   := '2004-01-20'; SG.Cells[1,I]   := '601.42'; SG.Cells[2,I]   := '528.66'; SG.Cells[3,I]   := '447.92'; SG.Cells[4,I]   := '676.96'; SG.Cells[5,I]   := '224300000';Inc(I);
    SG.Cells[0,I]   := '2004-01-21'; SG.Cells[1,I]   := '522.77'; SG.Cells[2,I]   := '623.62'; SG.Cells[3,I]   := '453.11'; SG.Cells[4,I]   := '665.72'; SG.Cells[5,I]   := '214920000';Inc(I);
    SG.Cells[0,I]   := '2004-01-22'; SG.Cells[1,I]   := '624.22'; SG.Cells[2,I]   := '623.18'; SG.Cells[3,I]   := '545.03'; SG.Cells[4,I]   := '717.41'; SG.Cells[5,I]   := '219720000';Inc(I);
    SG.Cells[0,I]   := '2004-01-23'; SG.Cells[1,I]   := '625.25'; SG.Cells[2,I]   := '568.29'; SG.Cells[3,I]   := '490.14'; SG.Cells[4,I]   := '691.77'; SG.Cells[5,I]   := '234260000';Inc(I);
    SG.Cells[0,I]   := '2004-01-26'; SG.Cells[1,I]   := '568.12'; SG.Cells[2,I]   := '702.51'; SG.Cells[3,I]   := '510.44'; SG.Cells[4,I]   := '725.18'; SG.Cells[5,I]   := '186170000';Inc(I);
    SG.Cells[0,I]   := '2004-01-27'; SG.Cells[1,I]   := '701.11'; SG.Cells[2,I]   := '609.92'; SG.Cells[3,I]   := '579.33'; SG.Cells[4,I]   := '748.81'; SG.Cells[5,I]   := '206560000';Inc(I);
    SG.Cells[0,I]   := '2004-01-28'; SG.Cells[1,I]   := '610.07'; SG.Cells[2,I]   := '468.37'; SG.Cells[3,I]   := '412.44'; SG.Cells[4,I]   := '703.25'; SG.Cells[5,I]   := '247660000';Inc(I);
    SG.Cells[0,I]   := '2004-01-29'; SG.Cells[1,I]   := '467.41'; SG.Cells[2,I]   := '510.29'; SG.Cells[3,I]   := '369.92'; SG.Cells[4,I]   := '611.56'; SG.Cells[5,I]   := '273970000';Inc(I);
    SG.Cells[0,I]   := '2004-01-30'; SG.Cells[1,I]   := '510.22'; SG.Cells[2,I]   := '488.07'; SG.Cells[3,I]   := '385.56'; SG.Cells[4,I]   := '551.03'; SG.Cells[5,I]   := '208990000';Inc(I);
    SG.Cells[0,I]   := '2004-02-02'; SG.Cells[1,I]   := '487.78'; SG.Cells[2,I]   := '499.18'; SG.Cells[3,I]   := '395.55'; SG.Cells[4,I]   := '614.44'; SG.Cells[5,I]   := '224800000';Inc(I);
    SG.Cells[0,I]   := '2004-02-03'; SG.Cells[1,I]   := '499.48'; SG.Cells[2,I]   := '505.18'; SG.Cells[3,I]   := '414.15'; SG.Cells[4,I]   := '571.48'; SG.Cells[5,I]   := '183810000';Inc(I);
    SG.Cells[0,I]   := '2004-02-04'; SG.Cells[1,I]   := '503.11'; SG.Cells[2,I]   := '470.74'; SG.Cells[3,I]   := '394.81'; SG.Cells[4,I]   := '567.85'; SG.Cells[5,I]   := '227760000';Inc(I);
    SG.Cells[0,I]   := '2004-02-05'; SG.Cells[1,I]   := '469.33'; SG.Cells[2,I]   := '495.55'; SG.Cells[3,I]   := '399.92'; SG.Cells[4,I]   := '566.37'; SG.Cells[5,I]   := '187810000';Inc(I);
    SG.Cells[0,I]   := '2004-02-06'; SG.Cells[1,I]   := '494.89'; SG.Cells[2,I]   := '593.03'; SG.Cells[3,I]   := '433.74'; SG.Cells[4,I]   := '634.81'; SG.Cells[5,I]   := '182880000';Inc(I);
    SG.Cells[0,I]   := '2004-02-09'; SG.Cells[1,I]   := '592.41'; SG.Cells[2,I]   := '579.03'; SG.Cells[3,I]   := '433.72'; SG.Cells[4,I]   := '634.81'; SG.Cells[5,I]   := '160720000';Inc(I);
    SG.Cells[0,I]   := '2004-02-10'; SG.Cells[1,I]   := '578.74'; SG.Cells[2,I]   := '613.85'; SG.Cells[3,I]   := '511.18'; SG.Cells[4,I]   := '667.03'; SG.Cells[5,I]   := '160590000';Inc(I);
    SG.Cells[0,I]   := '2004-02-11'; SG.Cells[1,I]   := '605.48'; SG.Cells[2,I]   := '737.72'; SG.Cells[3,I]   := '561.55'; SG.Cells[4,I]   := '779.41'; SG.Cells[5,I]   := '277850000';Inc(I);
    SG.Cells[0,I]   := '2004-02-12'; SG.Cells[1,I]   := '735.18'; SG.Cells[2,I]   := '694.07'; SG.Cells[3,I]   := '636.44'; SG.Cells[4,I]   := '775.03'; SG.Cells[5,I]   := '197560000';Inc(I);
    SG.Cells[0,I]   := '2004-02-13'; SG.Cells[1,I]   := '696.22'; SG.Cells[2,I]   := '627.85'; SG.Cells[3,I]   := '578.66'; SG.Cells[4,I]   := '755.47'; SG.Cells[5,I]   := '208340000';Inc(I);
    SG.Cells[0,I]   := '2004-02-17'; SG.Cells[1,I]   := '628.88'; SG.Cells[2,I]   := '714.88'; SG.Cells[3,I]   := '628.88'; SG.Cells[4,I]   := '762.07'; SG.Cells[5,I]   := '169730000';Inc(I);
    SG.Cells[0,I]   := '2004-02-18'; SG.Cells[1,I]   := '706.68'; SG.Cells[2,I]   := '671.99'; SG.Cells[3,I]   := '623.62'; SG.Cells[4,I]   := '764.36'; SG.Cells[5,I]   := '164370000';Inc(I);
    SG.Cells[0,I]   := '2004-02-19'; SG.Cells[1,I]   := '674.59'; SG.Cells[2,I]   := '664.73'; SG.Cells[3,I]   := '626.44'; SG.Cells[4,I]   := '794.95'; SG.Cells[5,I]   := '219890000';Inc(I);
    SG.Cells[0,I]   := '2004-02-20'; SG.Cells[1,I]   := '666.29'; SG.Cells[2,I]   := '619.03'; SG.Cells[3,I]   := '559.11'; SG.Cells[4,I]   := '722.77'; SG.Cells[5,I]   := '220560000';Inc(I);
    SG.Cells[0,I]   := '2004-02-23'; SG.Cells[1,I]   := '619.55'; SG.Cells[2,I]   := '609.62'; SG.Cells[3,I]   := '508.89'; SG.Cells[4,I]   := '711.84'; SG.Cells[5,I]   := '229950000';Inc(I);
    SG.Cells[0,I]   := '2004-02-24'; SG.Cells[1,I]   := '609.55'; SG.Cells[2,I]   := '566.37'; SG.Cells[3,I]   := '479.33'; SG.Cells[4,I]   := '681.41'; SG.Cells[5,I]   := '225670000';Inc(I);
    SG.Cells[0,I]   := '2004-02-25'; SG.Cells[1,I]   := '566.59'; SG.Cells[2,I]   := '601.62'; SG.Cells[3,I]   := '509.42'; SG.Cells[4,I]   := '660.73'; SG.Cells[5,I]   := '192420000';Inc(I);
    SG.Cells[0,I]   := '2004-02-26'; SG.Cells[1,I]   := '598.14'; SG.Cells[2,I]   := '580.14'; SG.Cells[3,I]   := '493.71'; SG.Cells[4,I]   := '652.96'; SG.Cells[5,I]   := '223230000';Inc(I);
    SG.Cells[0,I]   := '2004-02-27'; SG.Cells[1,I]   := '581.55'; SG.Cells[2,I]   := '583.92'; SG.Cells[3,I]   := '519.03'; SG.Cells[4,I]   := '689.55'; SG.Cells[5,I]   := '200050000';

end;

procedure TForm1.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    //
    if (X>600)and(Y>600) then begin
        dwSetPCMode(self);
    end else begin
        dwSetMobileMode(self,414,736);
        Height  := Max(400,Height);
    end;
end;

end.
