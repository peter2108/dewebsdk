object Form1: TForm1
  Left = 0
  Top = 0
  VertScrollBar.Visible = False
  AlphaBlend = True
  BorderStyle = bsNone
  Caption = 'DeWeb'
  ClientHeight = 601
  ClientWidth = 800
  Color = clWhite
  TransparentColor = True
  TransparentColorValue = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnMouseUp = FormMouseUp
  PixelsPerInch = 96
  TextHeight = 21
  object SG: TStringGrid
    AlignWithMargins = True
    Left = 3
    Top = 56
    Width = 794
    Height = 542
    HelpType = htKeyword
    HelpKeyword = 'vecandle'
    Margins.Top = 10
    Align = alClient
    ColCount = 6
    RowCount = 38
    TabOrder = 0
    RowHeights = (
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24
      24)
  end
  object Panel_0_Title: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 794
    Height = 40
    Hint = '{"dwstyle":"border-bottom:solid 1px #ffcc00;"}'
    Align = alTop
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 746
      Height = 30
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      Caption = 'DeWeb Candle'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitTop = -4
      ExplicitWidth = 784
      ExplicitHeight = 40
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 755
      Top = 2
      Width = 32
      Height = 32
      Hint = '{"type":"success","fontsize":"16px","icon":"el-icon-refresh"}'
      Margins.Top = 2
      Margins.Bottom = 2
      Align = alRight
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = Button1Click
    end
  end
end
