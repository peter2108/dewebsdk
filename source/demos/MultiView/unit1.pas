﻿unit unit1;

interface

uses
    //
    dwBase,
    dwMultiView,

    //
    CloneComponents,
    SynCommons,

    //
    Winapi.Windows, Winapi.Messages, Vcl.Forms, Vcl.Controls, Vcl.StdCtrls, System.Classes,
    DateUtils,SysUtils,Vcl.ExtCtrls, Vcl.Grids, Vcl.ComCtrls, VclTee.TeeGDIPlus, Vcl.Menus,
    VCLTee.TeEngine, VCLTee.TeeProcs, VCLTee.Chart, Vcl.Buttons, Data.DB, Data.Win.ADODB,
    Vcl.Imaging.pngimage, Vcl.Samples.Spin;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Panel_0_Banner: TPanel;
    Label_Introduce: TLabel;
    Panel_Title: TPanel;
    Label_Title: TLabel;
    Panel_Card: TPanel;
    Label_name: TLabel;
    Label_Addr: TLabel;
    Label1: TLabel;
    Label_Province: TLabel;
    Label_Sex: TLabel;
    Image_avatar: TImage;
    ADOQuery1: TADOQuery;
    Button_ID: TButton;
    ComboBox_HeadShip: TComboBox;
    SpinEdit_Salary: TSpinEdit;
    Label_Salary: TLabel;
    Edit_Province: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    gsMainDir   : String;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}


procedure TForm1.FormShow(Sender: TObject);
begin
    ADOQuery1.SQL.Text  := 'SELECT * FROM dw_Member';
    ADOQuery1.Open;
    //
    dwDataToViews(ADOQuery1,Panel_Card,
        '{"align":0,"items":['
            +'{"field":"AName","type":"string","component":"Label_Name"}'
            +',{"field":"HeadShip","type":"string","format":"","component":"ComboBox_HeadShip"}'
            +',{"field":"Sex","type":"boolean","format":["男","女"],"component":"Label_Sex"}'
            +',{"field":"Province","type":"string","format":"%s省","component":"Edit_Province"}'
            +',{"field":"Addr","type":"string","format":"","component":"Label_Addr"}'
            +',{"field":"Salary","type":"integer","format":"","component":"SpinEdit_Salary"}'
            +',{"field":"ID","type":"integer","format":"工号：%.4d","component":"Button_ID"}'
            +',{"field":"Photo","type":"image","format":"media/images/dwms/u%s.png","component":"Image_Avatar"}'
        +']}');

end;

end.
